# GitLab Semgrep Plus Rules Update

Checks [GitLab Semgrep Plus](https://gitlab.com/gitlab-ci-utils/gitlab-semgrep-plus) for any applicable [Semgrep rules](https://github.com/returntocorp/semgrep-rules) updates in the given commit and pushes updates back to GitLab Semgrep Plus. This is run in a separate project to isolate credentials that can update the repository from the project making the updates.

For details on the rule update job and LICENSE, see the [GitLab Semgrep Plus](https://gitlab.com/gitlab-ci-utils/gitlab-semgrep-plus) project.
